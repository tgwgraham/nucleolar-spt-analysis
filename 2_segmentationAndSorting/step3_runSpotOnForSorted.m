%%
% Location of Spot-On MATLAB package (available at
% https://gitlab.com/tjian-darzacq-lab/Spot-On)
addpath 'C:/Users/User/Dropbox/Tjian_Darzacq/code/SpotOn/spot-on-matlab/SpotOn_package/';

%%  % % % % % % Make list of mat files for each compartment % % % % % % % %

workspaces = {{},{},{}}; % list of mat files for compartments 0, 1, and 2
allTrajectories = {};

cellsToInclude = 1:20; 

for j=0:2
    for c = 1:numel(cellsToInclude) % range of cell numbers
        k = cellsToInclude(c);
        currfname = ['sortedTrajectories/' num2str(k) '/' num2str(j) '.mat'];
        if exist(currfname,'file')
            % compartment 0 = outside nucleus = index 1
            % compartment 1 = nucleoplasm = index 2
            % compartment 2 = nucleoli = index 3            
            workspaces{j + 1}{end+1} = currfname;
            allTrajectories{end+1} = currfname;
        end
    end
end

sampleNames = {'Cytoplasm','Nucleus','Nucleolus'}; % compartment names

%% Run Spot-On for nucleolus

j = 3; % compartment number

data_struct = struct([]);

%%%%% Acquisition Parameters: 
TimeGap = 7.5; % delay between frames in milliseconds
dZ = 0.700; % The axial observation slice in micrometers; Rougly 0.7 um for the example data (HiLo)
GapsAllowed = 0; % The number of allowed gaps in the tracking

%%%%% Data Processing Parameters:
TimePoints = 8; % How many delays to consider: N timepoints yield N-1 delays
BinWidth = 0.010; % Bin Width for computing histogram in micrometers (only for PDF; Spot-On uses 1 nm bins for CDF)
UseEntireTraj = 0; % If UseEntireTraj=1, all displacements from all trajectories will be used; If UseEntireTraj=0, only the first X displacements will be used. NB. this variable was previously called UseAllTraj but has been renamed UseEntireTraj
JumpsToConsider = 4; % If UseEntireTraj=0, the first JumpsToConsiders displacements for each dT where possible will be used. 
MaxJumpPlotPDF = 1.05; % the cut-off for displaying the displacement histograms plots
MaxJumpPlotCDF = 3.05; % the cut-off for displaying the displacement CDF plots
MaxJump = 5.05; % the overall maximal displacements to consider in micrometers
SavePlot = 1; % if SavePlot=1, key output plots will be saved to the folder "SavedPlots"; Otherwise set SavePlot = 0;
DoPlots = 1; % if DoPlots=1, Spot-On will output plots, but not if it's zero. Avoiding plots speeds up Spot-On for batch analysis

%%%%% Model Fitting Parameters:
ModelFit = 2; %Use 1 for PDF-fitting; Use 2 for CDF-fitting
DoSingleCellFit = 0; %Set to 1 if you want to analyse all single cells individually (slow). 
NumberOfStates = 2; % If NumberOfStates=2, a 2-state model will be used; If NumberOfStates=3, a 3-state model will be used 
FitIterations = 2; % Input the desired number of fitting iterations (random initial parameter guess for each)
FitLocError = 1; % If FitLocError=1, the localization error will fitted from the data
FitLocErrorRange = [0.010 0.075]; % min/max for model-fitted localization error in micrometers.
LocError = 0.035; % If FitLocError=0, LocError in units of micrometers will be used. 
UseWeights = 0; % If UseWeights=0, all TimePoints are given equal weights. If UseWeights=1, TimePoints are weighted according to how much data there is. E.g. 1dT will be weighted more than 5dT.
D_Free_2State = [0.2 25]; % min/max Diffusion constant for Free state in 2-state model (units um^2/s)
D_Bound_2State = [0.0001 0.05]; % min/max Diffusion constant for Bound state in 2-state model (units um^2/s)
D_Free1_3State = [0.5 25]; % min/max Diffusion constant #1 for Free state in 3-state model (units um^2/s)
D_Free2_3State = [0.5 25]; % min/max Diffusion constant #2 for Free state in 3-state model (units um^2/s)
D_Bound_3State = [0.0001 0.05]; % min/max Diffusion constant for Bound state in 3-state model (units um^2/s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%% DEFINE DATA SET PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%
data_struct(1).path = [pwd, filesep];
data_struct(1).workspaces = workspaces{j};
data_struct(1).workspaces = workspaces{j};
data_struct(1).Include = 1:numel(workspaces{j});
 

%%%%%%%%%%%%%%%%%%%%%%%%% SpotOn core mechanics %%%%%%%%%%%%%%%%%%%%%%%%%%%
Params = struct(); % Use Params to feed all the relevant data/parameters into the relevant functions
Params.TimeGap = TimeGap; Params.dZ = dZ; Params.GapsAllowed = GapsAllowed; Params.TimePoints = TimePoints; Params.BinWidth = BinWidth; Params.UseEntireTraj = UseEntireTraj; Params.DoPlots = DoPlots; Params.UseWeights = UseWeights;
Params.JumpsToConsider = JumpsToConsider; Params.MaxJumpPlotPDF = MaxJumpPlotPDF; Params.MaxJumpPlotCDF = MaxJumpPlotCDF; Params.MaxJump = MaxJump; Params.SavePlot = SavePlot; Params.ModelFit = ModelFit;
Params.DoSingleCellFit = DoSingleCellFit; Params.FitIterations = FitIterations; Params.FitLocError = FitLocError; Params.FitLocErrorRange = FitLocErrorRange; Params.LocError = LocError; Params.NumberOfStates = NumberOfStates;
Params.D_Free_2State = D_Free_2State; Params.D_Bound_2State = D_Bound_2State; Params.D_Free1_3State = D_Free1_3State; Params.D_Free2_3State = D_Free2_3State; Params.D_Bound_3State = D_Bound_3State;
Params.curr_dir = pwd; Params.SampleName = sampleNames{j}; Params.data_struct = data_struct;

[Output_struct] = SpotOn_core(Params);

save(['SavedPlots' filesep Params.SampleName '_output.mat'],'Output_struct');

% % % % % % reformat ugly figure #4

% write out information to a plain text in folder SavedPlots
fh = fopen(['SavedPlots' filesep Params.SampleName '_results.txt'],'w');
longTitle = get(gca,'Title');
longTitle = longTitle.String;
for j=1:numel(longTitle)
    disp(longTitle{j})
    fprintf(fh,'%s\n',longTitle{j})
end
fclose(fh)

% replace ugly long title with sample name
longTitle = get(gca,'Title');
longTitle.String = Params.SampleName;

% resize figure
set(gcf,'Position',[4.8854    4.7917    4.4063    5.9688])
set(gca,'FontName','Arial','FontSize',12)

saveas(gcf,['SavedPlots' filesep Params.SampleName 'pdfPlot.fig'])
% saveas(gcf,['SavedPlots' filesep Params.SampleName 'pdfPlot.pdf'])
saveas(gcf,['SavedPlots' filesep Params.SampleName 'pdfPlot.png'])


%% Run Spot-On for nucleoplasm

j = 2; % compartment number

data_struct = struct([]);

%%%%% Acquisition Parameters: 
TimeGap = 7.5; % delay between frames in milliseconds
dZ = 0.700; % The axial observation slice in micrometers; Rougly 0.7 um for the example data (HiLo)
GapsAllowed = 0; % The number of allowed gaps in the tracking

%%%%% Data Processing Parameters:
TimePoints = 8; % How many delays to consider: N timepoints yield N-1 delays
BinWidth = 0.010; % Bin Width for computing histogram in micrometers (only for PDF; Spot-On uses 1 nm bins for CDF)
UseEntireTraj = 0; % If UseEntireTraj=1, all displacements from all trajectories will be used; If UseEntireTraj=0, only the first X displacements will be used. NB. this variable was previously called UseAllTraj but has been renamed UseEntireTraj
JumpsToConsider = 4; % If UseEntireTraj=0, the first JumpsToConsiders displacements for each dT where possible will be used. 
MaxJumpPlotPDF = 1.05; % the cut-off for displaying the displacement histograms plots
MaxJumpPlotCDF = 3.05; % the cut-off for displaying the displacement CDF plots
MaxJump = 5.05; % the overall maximal displacements to consider in micrometers
SavePlot = 1; % if SavePlot=1, key output plots will be saved to the folder "SavedPlots"; Otherwise set SavePlot = 0;
DoPlots = 1; % if DoPlots=1, Spot-On will output plots, but not if it's zero. Avoiding plots speeds up Spot-On for batch analysis

%%%%% Model Fitting Parameters:
ModelFit = 2; %Use 1 for PDF-fitting; Use 2 for CDF-fitting
DoSingleCellFit = 0; %Set to 1 if you want to analyse all single cells individually (slow). 
NumberOfStates = 2; % If NumberOfStates=2, a 2-state model will be used; If NumberOfStates=3, a 3-state model will be used 
FitIterations = 2; % Input the desired number of fitting iterations (random initial parameter guess for each)
FitLocError = 1; % If FitLocError=1, the localization error will fitted from the data
FitLocErrorRange = [0.010 0.075]; % min/max for model-fitted localization error in micrometers.
LocError = 0.035; % If FitLocError=0, LocError in units of micrometers will be used. 
UseWeights = 0; % If UseWeights=0, all TimePoints are given equal weights. If UseWeights=1, TimePoints are weighted according to how much data there is. E.g. 1dT will be weighted more than 5dT.
D_Free_2State = [0.5 25]; % min/max Diffusion constant for Free state in 2-state model (units um^2/s)
D_Bound_2State = [0.0001 0.05]; % min/max Diffusion constant for Bound state in 2-state model (units um^2/s)
D_Free1_3State = [0.5 25]; % min/max Diffusion constant #1 for Free state in 3-state model (units um^2/s)
D_Free2_3State = [0.5 25]; % min/max Diffusion constant #2 for Free state in 3-state model (units um^2/s)
D_Bound_3State = [0.0001 0.05]; % min/max Diffusion constant for Bound state in 3-state model (units um^2/s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%% DEFINE DATA SET PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%
data_struct(1).path = [pwd, filesep];
data_struct(1).workspaces = workspaces{j};
data_struct(1).workspaces = workspaces{j};
data_struct(1).Include = 1:numel(workspaces{j});
 

%%%%%%%%%%%%%%%%%%%%%%%%% SpotOn core mechanics %%%%%%%%%%%%%%%%%%%%%%%%%%%
Params = struct(); % Use Params to feed all the relevant data/parameters into the relevant functions
Params.TimeGap = TimeGap; Params.dZ = dZ; Params.GapsAllowed = GapsAllowed; Params.TimePoints = TimePoints; Params.BinWidth = BinWidth; Params.UseEntireTraj = UseEntireTraj; Params.DoPlots = DoPlots; Params.UseWeights = UseWeights;
Params.JumpsToConsider = JumpsToConsider; Params.MaxJumpPlotPDF = MaxJumpPlotPDF; Params.MaxJumpPlotCDF = MaxJumpPlotCDF; Params.MaxJump = MaxJump; Params.SavePlot = SavePlot; Params.ModelFit = ModelFit;
Params.DoSingleCellFit = DoSingleCellFit; Params.FitIterations = FitIterations; Params.FitLocError = FitLocError; Params.FitLocErrorRange = FitLocErrorRange; Params.LocError = LocError; Params.NumberOfStates = NumberOfStates;
Params.D_Free_2State = D_Free_2State; Params.D_Bound_2State = D_Bound_2State; Params.D_Free1_3State = D_Free1_3State; Params.D_Free2_3State = D_Free2_3State; Params.D_Bound_3State = D_Bound_3State;
Params.curr_dir = pwd; Params.SampleName = sampleNames{j}; Params.data_struct = data_struct;

[Output_struct] = SpotOn_core(Params);

save(['SavedPlots' filesep Params.SampleName '_output.mat'],'Output_struct');

% % % % % % reformat ugly figure #4

% write out information to a plain text in folder SavedPlots
fh = fopen(['SavedPlots' filesep Params.SampleName '_results.txt'],'w');
longTitle = get(gca,'Title');
longTitle = longTitle.String;
for j=1:numel(longTitle)
    disp(longTitle{j})
    fprintf(fh,'%s\n',longTitle{j})
end
fclose(fh)

% replace ugly long title with sample name
longTitle = get(gca,'Title');
longTitle.String = Params.SampleName;

% resize figure
set(gcf,'Position',[4.8854    4.7917    4.4063    5.9688])
set(gca,'FontName','Arial','FontSize',12)

saveas(gcf,['SavedPlots' filesep Params.SampleName 'pdfPlot.fig'])
% saveas(gcf,['SavedPlots' filesep Params.SampleName 'pdfPlot.pdf'])
saveas(gcf,['SavedPlots' filesep Params.SampleName 'pdfPlot.png'])

%% Run Spot-On for all trajectories

data_struct = struct([]);

%%%%% Acquisition Parameters: 
TimeGap = 7.5; % delay between frames in milliseconds
dZ = 0.700; % The axial observation slice in micrometers; Rougly 0.7 um for the example data (HiLo)
GapsAllowed = 0; % The number of allowed gaps in the tracking

%%%%% Data Processing Parameters:
TimePoints = 8; % How many delays to consider: N timepoints yield N-1 delays
BinWidth = 0.010; % Bin Width for computing histogram in micrometers (only for PDF; Spot-On uses 1 nm bins for CDF)
UseEntireTraj = 0; % If UseEntireTraj=1, all displacements from all trajectories will be used; If UseEntireTraj=0, only the first X displacements will be used. NB. this variable was previously called UseAllTraj but has been renamed UseEntireTraj
JumpsToConsider = 4; % If UseEntireTraj=0, the first JumpsToConsiders displacements for each dT where possible will be used. 
MaxJumpPlotPDF = 1.05; % the cut-off for displaying the displacement histograms plots
MaxJumpPlotCDF = 3.05; % the cut-off for displaying the displacement CDF plots
MaxJump = 5.05; % the overall maximal displacements to consider in micrometers
SavePlot = 1; % if SavePlot=1, key output plots will be saved to the folder "SavedPlots"; Otherwise set SavePlot = 0;
DoPlots = 1; % if DoPlots=1, Spot-On will output plots, but not if it's zero. Avoiding plots speeds up Spot-On for batch analysis

%%%%% Model Fitting Parameters:
ModelFit = 2; %Use 1 for PDF-fitting; Use 2 for CDF-fitting
DoSingleCellFit = 0; %Set to 1 if you want to analyse all single cells individually (slow). 
NumberOfStates = 2; % If NumberOfStates=2, a 2-state model will be used; If NumberOfStates=3, a 3-state model will be used 
FitIterations = 2; % Input the desired number of fitting iterations (random initial parameter guess for each)
FitLocError = 1; % If FitLocError=1, the localization error will fitted from the data
FitLocErrorRange = [0.010 0.075]; % min/max for model-fitted localization error in micrometers.
LocError = 0.035; % If FitLocError=0, LocError in units of micrometers will be used. 
UseWeights = 0; % If UseWeights=0, all TimePoints are given equal weights. If UseWeights=1, TimePoints are weighted according to how much data there is. E.g. 1dT will be weighted more than 5dT.
D_Free_2State = [0.5 25]; % min/max Diffusion constant for Free state in 2-state model (units um^2/s)
D_Bound_2State = [0.0001 0.05]; % min/max Diffusion constant for Bound state in 2-state model (units um^2/s)
D_Free1_3State = [0.5 25]; % min/max Diffusion constant #1 for Free state in 3-state model (units um^2/s)
D_Free2_3State = [0.5 25]; % min/max Diffusion constant #2 for Free state in 3-state model (units um^2/s)
D_Bound_3State = [0.0001 0.05]; % min/max Diffusion constant for Bound state in 3-state model (units um^2/s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%% DEFINE DATA SET PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%
data_struct(1).path = [pwd, filesep];
data_struct(1).workspaces = allTrajectories;
data_struct(1).Include = 1:numel(allTrajectories);
 

%%%%%%%%%%%%%%%%%%%%%%%%% SpotOn core mechanics %%%%%%%%%%%%%%%%%%%%%%%%%%%
Params = struct(); % Use Params to feed all the relevant data/parameters into the relevant functions
Params.TimeGap = TimeGap; Params.dZ = dZ; Params.GapsAllowed = GapsAllowed; Params.TimePoints = TimePoints; Params.BinWidth = BinWidth; Params.UseEntireTraj = UseEntireTraj; Params.DoPlots = DoPlots; Params.UseWeights = UseWeights;
Params.JumpsToConsider = JumpsToConsider; Params.MaxJumpPlotPDF = MaxJumpPlotPDF; Params.MaxJumpPlotCDF = MaxJumpPlotCDF; Params.MaxJump = MaxJump; Params.SavePlot = SavePlot; Params.ModelFit = ModelFit;
Params.DoSingleCellFit = DoSingleCellFit; Params.FitIterations = FitIterations; Params.FitLocError = FitLocError; Params.FitLocErrorRange = FitLocErrorRange; Params.LocError = LocError; Params.NumberOfStates = NumberOfStates;
Params.D_Free_2State = D_Free_2State; Params.D_Bound_2State = D_Bound_2State; Params.D_Free1_3State = D_Free1_3State; Params.D_Free2_3State = D_Free2_3State; Params.D_Bound_3State = D_Bound_3State;
Params.curr_dir = pwd; Params.SampleName = 'All_Trajectories_'; Params.data_struct = data_struct;

[Output_struct] = SpotOn_core(Params);

save(['SavedPlots' filesep Params.SampleName '_output.mat'],'Output_struct');

% % % % % % reformat ugly figure #4

% write out information to a plain text in folder SavedPlots
fh = fopen(['SavedPlots' filesep Params.SampleName '_results.txt'],'w');
longTitle = get(gca,'Title');
longTitle = longTitle.String;
for j=1:numel(longTitle)
    disp(longTitle{j})
    fprintf(fh,'%s\n',longTitle{j})
end
fclose(fh)

% replace ugly long title with sample name
longTitle = get(gca,'Title');
longTitle.String = Params.SampleName;

% resize figure
set(gcf,'Position',[4.8854    4.7917    4.4063    5.9688])
set(gca,'FontName','Arial','FontSize',12)

saveas(gcf,['SavedPlots' filesep Params.SampleName 'pdfPlot.fig'])
% saveas(gcf,['SavedPlots' filesep Params.SampleName 'pdfPlot.pdf'])
saveas(gcf,['SavedPlots' filesep Params.SampleName 'pdfPlot.png'])









