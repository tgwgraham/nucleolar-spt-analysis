%% sort trajectories as nuclear or nucleoplasmic

cellsToInclude = 1:20; % List of cell trajectory file numbers to include.

mkdir sortedTrajectories

timestep = 0.00748;
sizePerPx = 0.16;

for j = 1:numel(cellsToInclude)
    
    disp(j)

    movieNum = cellsToInclude(j);
    masks = load(['realMasks/realMasks_' num2str(movieNum) '.mat']);
    basefname = sprintf('../D1_mNG-EN_EFH_20nM_PA-JF646_200nM_JFX549_1ms-AOTF100-1100mW-633nm_7msCam_cell%02d',movieNum);
    labelMatrix = masks.nuclearMask + masks.nucleolarMask;
    
    trackedPar = csvToTrackedParWithCompartment(basefname, timestep, ...
          sizePerPx, labelMatrix);

    outfname = ['sortedTrajectories', filesep, num2str(movieNum), filesep];
    mkdir(outfname)
    sortedTP = sortTrackedParByCompartment(trackedPar,outfname);
    
end

