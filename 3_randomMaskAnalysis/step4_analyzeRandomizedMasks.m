addpath ../nucleolar_segmentation

% Location of Spot-On MATLAB package (available at
% https://gitlab.com/tjian-darzacq-lab/Spot-On)
addpath 'C:/Users/User/Dropbox/Tjian_Darzacq/code/SpotOn/spot-on-matlab/SpotOn_package/';

%% run Spot-On for many replicates with randomized masks

nreplicates = 200;
randMaskDirectory = '../randMasks/';
realMaskDirectory = '../realMasks/';
cellsToInclude = 1:20; 
timestep = 0.00748;
sizePerPx = 0.16;
moviefname = '../../D1_mNG-EN_EFH_20nM_PA-JF646_200nM_JFX549_1ms-AOTF100-1100mW-633nm_7msCam_cell%02d';

for r=1:nreplicates
    
    % make a folder for this bootstrap replicate
    foldername = ['bootstrapReplicates/' num2str(r)];
    mkdir(['bootstrapReplicates/' num2str(r)])

    
    % select random masks to use
    fnums = 1000:100:9900;
    maskNums = fnums(randi(numel(fnums),[numel(cellsToInclude),1]));
    
    % write out a record of which random masks you've chosen
    save([foldername '/maskNums.mat'], 'maskNums');
    
   
    % sort trajectories based on these randomized masks, and write out
    % sorted trajectories in the corresponding bootstrap replicates folder 
    for j = 1:numel(cellsToInclude)
        movieNum = cellsToInclude(j);
        
        randMask = load([randMaskDirectory filesep num2str(movieNum) filesep num2str(maskNums(j)) '.mat']);
        realMask = load([realMaskDirectory filesep 'realMasks_' num2str(movieNum) '.mat']);
        basefname = sprintf(moviefname,movieNum);
        labelMatrix = realMask.nuclearMask + (randMask.mockNucleolarMask == 2);

        trackedPar = csvToTrackedParWithCompartment(basefname, timestep, ...
              sizePerPx, labelMatrix);

        outfname = [foldername, filesep, 'sortedTrajectories', filesep, num2str(movieNum), filesep];
        mkdir(outfname)
        sortedTP = sortTrackedParByCompartment(trackedPar,outfname);
        
    end
    
    % Run Spot-On
    disp(['Analyzing replicate ' num2str(r)])
    outdir = ['bootstrapReplicates/' num2str(r) '/spotOnOutput'];
    mkdir(outdir)
	runSpotOnForRandomized(r,outdir); 
end

