%%
% Location of Spot-On MATLAB package (available at
% https://gitlab.com/tjian-darzacq-lab/Spot-On)
addpath 'C:/Users/User/Dropbox/Tjian_Darzacq/code/SpotOn/spot-on-matlab/SpotOn_package/';

%% % % % % % Run Spot-On for data randomly resampled by cell % % % % % % %

outdir = 'bootstrapSamples';
mkdir(outdir);

sampleNames = {'Cytoplasm','Nucleus','Nucleolus'}; % compartment names
nreplicates = 200;

for r = 1:nreplicates
    workspaces = {{},{},{}}; % list of mat files for compartments 0, 1, and 2
    allTrajectories = {};

    cellsToInclude1 = randi(20,1,20);
    cellsToInclude2 = randi(20,1,20);
    trajdir1 = '../../200919/analysis20210519_rerun/sortedTrajectories/';
    trajdir2 = '../../200920/analysis20210519_rerun/sortedTrajectories/';

    % compartment 0 = outside nucleus = index 1
    % compartment 1 = nucleoplasm = index 2
    % compartment 2 = nucleoli = index 3            
    for j=0:2
        for c = 1:numel(cellsToInclude1) % range of cell numbers
            k = cellsToInclude1(c);
            currfname = [trajdir1 num2str(k) '/' num2str(j) '.mat'];
            if exist(currfname,'file')
                workspaces{j + 1}{end+1} = currfname;
                allTrajectories{end+1} = currfname;
            end
        end

        for c = 1:numel(cellsToInclude2) % range of cell numbers
            k = cellsToInclude2(c);
            currfname = [trajdir2 num2str(k) '/' num2str(j) '.mat'];
            if exist(currfname,'file')
                workspaces{j + 1}{end+1} = currfname;
                allTrajectories{end+1} = currfname;
            end
        end
    end

    for j=2:3
        data_struct = struct([]);
        data_struct(1).path = [pwd, filesep];
        data_struct(1).workspaces = workspaces{j};
        data_struct(1).Include = 1:numel(workspaces{j});
        Params = setSpotOnParams(data_struct);
        Params.SampleName = sampleNames{j};

        [Output_struct] = SpotOn_core(Params);

        save([outdir filesep Params.SampleName '_output' num2str(r) '.mat'],'Output_struct');
        csvwrite([outdir filesep Params.SampleName '_params' num2str(r) '.csv'],...
            Output_struct.merged_model_params)

    end
end




