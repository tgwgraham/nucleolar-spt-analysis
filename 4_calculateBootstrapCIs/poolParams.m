NP = zeros(200,4);
NL = zeros(200,4);

for j=1:200
    disp(j)
    a = load(sprintf('bootstrapSamples/Nucleus_output%d.mat',j));
    NP(j,:) = a.Output_struct.merged_model_params;
    
    a = load(sprintf('bootstrapSamples/Nucleolus_output%d.mat',j));
    NL(j,:) = a.Output_struct.merged_model_params;
end

save('bootstrap_fit_samples.mat','NL','NP');