function sumOfMasks = randomizeNucleolarMask(nuclearMask,nucleolarMask,outfname,...
    nTimeSteps,writeIncrement,varargin)
% sumOfMasks = randomizeNucleolarMask(nuclearMask,nucleolarMask,outfname,...
%     nTimeSteps,writeIncrement,varargin)
% 
% required inputs:
% nuclearMask - binary mask of nucleus
% nucleolarMask - binary mask of nucleoli
% outfname - base file name for output .mat files, which will be numbered
% by frame number
% nTimeSteps - number of Monte Carlo steps to take
% writeIncrement - how many frames between writing out .mat files
% 
% optional inputs
% 'displayOn' - set to 1 to display output of the simulation in real time
% 'displayIncrement' - number of steps per new display
% 'nRotationSteps' - number of discrete steps to break up the 360 degree
% rotation of each ROI into.
% 'teleportationFrequency' - frequency of instantly jumping to an empty
% location (decimal between 0 and 1)
% 'cmap' - color map for display (3 x 3 array)
% 
% output
% sumOfMasks - average of all of the masks generated during the simulation
% (to see how uniform the coverage is by the mock nucleoli)

defaultRotationSteps = 36;
defaultDisplayIncrement = 10;
% default frequency of getting "teleported" to an empty spot
defaultTeleportationFrequency = 0.01; 
defaultCmap = [[0.5,0,0]; [0,0,0]; [0,1,1]];




p = inputParser;
addRequired(p,'nuclearMask');
addRequired(p,'nucleolarMask');
addRequired(p,'outfname');
addRequired(p,'nTimeSteps');
addRequired(p,'writeIncrement');
addParameter(p,'displayOn',0);
addParameter(p,'displayIncrement',defaultDisplayIncrement);
addParameter(p,'nRotationSteps',defaultRotationSteps);
addParameter(p,'teleportationFrequency',defaultTeleportationFrequency);
addParameter(p,'cmap',defaultCmap);

parse(p,nuclearMask,nucleolarMask,outfname,nTimeSteps,writeIncrement,varargin{:});

displayOn = p.Results.displayOn;
displayIncrement = p.Results.displayIncrement;
nRotationSteps = p.Results.nRotationSteps;
teleportationFrequency = p.Results.teleportationFrequency;
cmap = p.Results.cmap;

% get individual nucleolar ROIs
cc = bwconncomp(nucleolarMask);
rp = regionprops(cc);


% for each connected component, make a series of rotated images of that ROI
sz = size(nucleolarMask);
rotations = {};
rotationIncrement = 360/nRotationSteps;
for j = 1:numel(cc.PixelIdxList)
    rotations{j} = {};
    currCC = cc.PixelIdxList{j};
    bb = rp(j).BoundingBox;
    currMask = zeros(sz);
    currMask(currCC) = 1;
    currMask = currMask(floor(bb(2)):floor(bb(2))+round(bb(4)),...
        floor(bb(1)):floor(bb(1))+round(bb(3)));
    rotations{j}{1} = currMask;
    for k=1:nRotationSteps-1
        rotations{j}{k+1} = imrotate(currMask,k*rotationIncrement);
    end
end

% width of border to use around the nuclear image to prevent rotated
% nucleolar masks from going over the edges
% set to be equal to 1 + the maximum distance from a centroid to the edge
% of a bounding box
borderWidth = 0; 

% calculate centroids of all rotated images relative to their bounding boxes
centroids = zeros(numel(cc.PixelIdxList),nRotationSteps,2);
nregions = numel(cc.PixelIdxList);
for j=1:nregions
    for k=1:nRotationSteps
        currMask = rotations{j}{k};
        [X,Y] = meshgrid(1:size(currMask,2),1:size(currMask,1)); % x = columns, y = rows
        currMaskSum = sum(sum(currMask));
        xCentroid = sum(sum(X.*currMask))/currMaskSum;
        yCentroid = sum(sum(Y.*currMask))/currMaskSum;
        centroids(j,k,1) = xCentroid;
        centroids(j,k,2) = yCentroid;
        % increase border width if necessary to keep nucleolar masks from
        % running off the edge.
        if xCentroid + 1 > borderWidth
            borderWidth = xCentroid + 1;
        elseif size(currMask,2) - xCentroid + 1> borderWidth
            borderWidth = size(currMask,2) - xCentroid + 1;
        elseif yCentroid + 1 > borderWidth
            borderWidth = yCentroid + 1;
        elseif size(currMask,2) - yCentroid + 1> borderWidth
            borderWidth = size(currMask,2) - yCentroid + 1;
        end
    end
end

borderWidth = ceil(borderWidth);

% % % % % % % % % % % % % % % % % % % % % % % % % % % 
% perform simulation steps

if displayOn == 1
    figure;
end

c = {rp.Centroid};
k = ones(1,nregions);

originalNM = single(nuclearMask);
originalNM = padarray(originalNM,[borderWidth,borderWidth]);
originalNM(originalNM == 0) = 1.5;
paddedSize = size(originalNM);

nrows = size(originalNM,1);
ncols = size(originalNM,2);

sumOfMasks = zeros(size(nuclearMask));

prevConfig = originalNM;

for n=1:nTimeSteps % loop over all time steps
    
    oldc = c;
    oldk = k;

    currConfig = originalNM;
    
    for j=1:nregions % loop over all nucleolus ROIs
        
        if rand < teleportationFrequency % stochastically choose to teleport to a new location
            freePixels = find(prevConfig == 1);
            chosenPixel = freePixels(randi(numel(freePixels)));
            [row,col] = ind2sub(paddedSize,chosenPixel);
            % Need to subtract borderWidth here.
            % Centroid positions are relative to the original (non-padded)
            % image and are thus offset by borderWidth.
            c{j}(2) = row - borderWidth;
            c{j}(1) = col - borderWidth;
        else % or else take a smaller step

            % with equal probability, move +1, 0, or -1 in x
            r = rand;
            if r < 0.33  % step in x
                c{j}(1) = c{j}(1) + 1;
            elseif r < 0.66
                c{j}(1) = c{j}(1) - 1;
            end

            % with equal probability, move +1, 0, or -1 in y
            r = rand;
            if r < 0.33
                c{j}(2) = c{j}(2) + 1;
            elseif r < 0.66
                c{j}(2) = c{j}(2) - 1;
            end
            
            r = rand;
            if r < 0.33
                k(j) = mod(k(j)+1,nRotationSteps);
            elseif r < 0.66
                k(j) = mod(k(j)-1,nRotationSteps);
            end

            if k(j) == 0
                k(j) = nRotationSteps-1;
            end
        end 
    end
    
    reject = 0;
    
    for j=1:nregions
        
        currMask = rotations{j}{k(j)};
        
        h = size(currMask,1);
        w = size(currMask,2);
        
        cx = round(c{j}(1));
        cy = round(c{j}(2));

        offsetx = round(centroids(j,k(j),1));
        offsety = round(centroids(j,k(j),2));
        
        if cy-offsety+1+borderWidth < 1
            reject = 1;
            disp('Warning: Edge collision type 1.')
            disp(cy-offsety+1+borderWidth)
            break;
        elseif cy-offsety+h+borderWidth > nrows
            reject = 1;
            disp('Warning: Edge collision type 2.')
            break;
        elseif cx-offsetx+1+borderWidth < 1
            reject = 1;
            disp('Warning: Edge collision type 3.')
            break;
        elseif cx-offsetx+w+borderWidth > ncols
            reject = 1;
            disp('Warning: Edge collision type 4.')
            break;
        else
        currConfig(cy-offsety+1+borderWidth:cy-offsety+h+borderWidth,...
            cx-offsetx+1+borderWidth:cx-offsetx+w+borderWidth) = ...
            currConfig(cy-offsety+1+borderWidth:cy-offsety+h+borderWidth,...
            cx-offsetx+1+borderWidth:cx-offsetx+w+borderWidth) + currMask;
        end

    end
    
    if sum(currConfig(:) > 2) ~= 0
        reject = 1;
    end
    
    if reject == 1
        c = oldc;
        k = oldk;
        currConfig = prevConfig;
    else
        sumOfMasks = sumOfMasks + currConfig(borderWidth+1:end-borderWidth,borderWidth+1:end-borderWidth);
    end
       
    if displayOn && mod(n,displayIncrement) == 1
        imagesc(currConfig(borderWidth+1:end-borderWidth,borderWidth+1:end-borderWidth)); 
        colormap(cmap)
        axis off;
        title(sprintf('Frame %d of %d',n,nTimeSteps));
        drawnow;
    end
    
    if mod(n,writeIncrement) == 0
        mockNucleolarMask = currConfig(borderWidth+1:end-borderWidth,borderWidth+1:end-borderWidth);
        save([outfname, num2str(n), '.mat'],'mockNucleolarMask')   
    end

    prevConfig = currConfig;
    
end


end