function trackedPar = csvToTrackedParWithCompartment(basefname, ...
    timestep, sizePerPx, labelMatrix)
% trackedPar = csvToTrackedParWithCompartment(basefname, timestep, ...
%     sizePerPx, labelMatrix)
% 
% Convert csv file from Alec's quot tracking code to a format that can be
% imported by the MATLAB version of SpotOn
% Includes an additional "Compartment" element of the data structure 
% indicating which subnuclear compartment each localization is in (to the
% nearest pixel)
% 
% inputs:
% basefname - file name; note: In this version, enter the file name with
% the extension (e.g., csv).
% timestep - time interval per frame
% sizePerPx - length per camera pixel in microns (e.g., 0.16)
% labelMatrix - 2D matrix, the same size as the image, giving a numerical
% label at each position. For instance, cytoplasmic pixels could be
% assigned 0, nucleoplasmic pixels 1, and nucleolar pixels 2.
% 

fname = [basefname, '.csv'];
trajs = csvread(fname,1);

xmax = size(labelMatrix,1);
ymax = size(labelMatrix,2);
x = min(max(trajs(:,1),1),xmax);
y = min(max(trajs(:,2),1),ymax);
x = round(x);
y = round(y);

indices = sub2ind(size(labelMatrix),x,y);

compartments = labelMatrix(indices);

% imagesc(lm);
% hold on;
% plot(y,x,'r.'); % x is rows; y is columns

trajIndices = trajs(:,18);
uniqueIndices = unique(trajIndices);

for j = 1:numel(uniqueIndices)
    selector = trajIndices == uniqueIndices(j);
    currRows = trajs(selector,:);
    trackedPar(j).xy = currRows(:,1:2) * sizePerPx;
    trackedPar(j).Frame = currRows(:,16);
    % there's probably a better way to do this by getting the timestamp
    % from the movie metadata:
    trackedPar(j).TimeStamp = currRows(:,16)*timestep; 
    
    trackedPar(j).Compartments = compartments(selector);
end

save([basefname '_trackedParWithCompartment.mat'], 'trackedPar')

end



